import 'dart:convert';

import 'package:apidemo/insertuserpage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiDemo extends StatefulWidget {
  const ApiDemo({Key? key}) : super(key: key);

  @override
  State<ApiDemo> createState() => _ApiDemoState();
}

class _ApiDemoState extends State<ApiDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Api Demo"),
        actions: [
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) {
                    return InsertUserPage();
                  },
                ),
              ).then(
                (value) {
                  setState(() {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Data Added Succesfully"),),);
                  });
                },
              );
            },
            child: Icon(Icons.add),
          ),
        ],
      ),
      body: FutureBuilder<http.Response>(
        builder: (context, snapshot) {
          if (snapshot != null && snapshot.hasData) {
            dynamic jsonData = jsonDecode(snapshot.data!.body.toString());
            return ListView.builder(
              itemCount: jsonData.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return InsertUserPage(map: jsonData[index]);
                        },
                      ),
                    ).then(
                      (value) {
                        setState(() {});
                      },
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.all(10),
                    child: ListTile(
                      tileColor: Color.fromRGBO(200, 230, 255, 0.8),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      title: Text(
                        jsonData[index]['name'].toString(),
                      ),
                      subtitle: Text(jsonData[index]['city'].toString()),
                      leading: CircleAvatar(
                        backgroundImage: NetworkImage(
                          jsonData[index]['avatar'],
                        ),
                      ),
                      trailing: InkWell(
                        onTap: () {
                          // deleteUser(jsonData[index]['id']).then((value) {
                          //   setState(() {
                          //
                          //   });
                          // },);
                          showAlertDialog(context, index, jsonData);
                        },
                        child: Icon(Icons.delete),
                      ),
                    ),
                  ),
                );
              },
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
        future: getStudentApi(),
      ),
    );
  }

  Future<http.Response> getStudentApi() async {
    http.Response res = await http
        .get(Uri.parse('https://6312f3f0a8d3f673ffc1a55b.mockapi.io/Student'));
    print("Data : ${res.body.toString()}");
    // Map<String, dynamic> map = jsonDecode(res.body.toString());
    // print("Map : ${map}");
    // Map map = {};
    // map["name"] = "Hello";
    // map["city"] = "Rajkot";
    // map['avatar'] = "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/454.jpg";
    //
    // var responce1 = await http.post(Uri.parse('https://6312f3f0a8d3f673ffc1a55b.mockapi.io/Student'),body: map);
    // print("Responce 1 : ${responce1}");
    return res;
  }

  Future<void> deleteUser(id) async {
    http.Response res = await http.delete(
        Uri.parse('https://6312f3f0a8d3f673ffc1a55b.mockapi.io/Student/$id'));
  }

  showAlertDialog(BuildContext context, index, jsonData) {
    Widget yesButton = TextButton(
      child: Text(
        "Yes",
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.8),
        ),
      ),
      onPressed: () async {
        deleteUser(jsonData[index]['id']).then(
          (value) {
            setState(() {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text("Delete Succesfully"),
                ),
              );
            });
          },
        );
        Navigator.pop(context);
        // setState(() {});
      },
    );
    Widget noButton = TextButton(
      child: Text(
        "No",
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.8),
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text(
        "Alert",
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.8),
        ),
      ),
      content: Text(
        "Are you sure want to delete?",
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.5),
        ),
      ),
      backgroundColor: Color.fromRGBO(80, 80, 80, 0.5),
      actions: [
        yesButton,
        noButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
