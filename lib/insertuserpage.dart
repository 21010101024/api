import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InsertUserPage extends StatefulWidget {
  InsertUserPage({Key? key, this.map}) : super(key: key);

  Map? map;

  @override
  State<InsertUserPage> createState() => _InsertUserPageState();
}

class _InsertUserPageState extends State<InsertUserPage> {
  var formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController imageController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  bool isLoading = false;
  @override
  void initState() {
    nameController.text = widget.map == null ? '' : widget.map!['name'];
    imageController.text = widget.map == null ? '' : widget.map!['avatar'];
    cityController.text = widget.map == null ? '' : widget.map!['city'];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add User"),
      ),
      body: Form(
        key: formKey,
        child: Column(
          children: [
            TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide(color: Colors.black),
                ),
                hintText: "Name",
              ),
              validator: (value) {
                if (value == null || value!.isEmpty) {
                  return "Enter The Username";
                }
              },
            ),
            TextFormField(
              controller: imageController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide(color: Colors.black),
                ),
                hintText: "Url",
              ),
              validator: (value) {
                if (value == null || value!.isEmpty) {
                  return "Enter The Image";
                }
              },
            ),
            TextFormField(
              controller: cityController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide(color: Colors.black),
                ),
                hintText: "City",
              ),
              validator: (value) {
                if (value == null || value!.isEmpty) {
                  return "Enter The City";
                }
              },
            ),
            InkWell(
              onTap: () {
                setState(() {
                  isLoading = true;
                });
                if (formKey.currentState!.validate()) {
                  if (widget.map == null) {
                    insertUser().then(
                      (value) {
                        Navigator.of(context).pop(true);
                      },
                    );
                  } else {
                    updateUser(widget.map!['id']).then(
                      (value) {
                        Navigator.of(context).pop(true);
                      },
                    );
                  }
                }
              },
              child: Container(
                padding:
                    EdgeInsets.only(top: 10, right: 50, bottom: 10, left: 50),
                decoration: BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: isLoading?CircularProgressIndicator():Text("Submit"),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> insertUser() async {
    Map map = {};
    map["name"] = nameController.text;
    map["city"] = cityController.text;
    map['avatar'] = imageController.text;
    var responce1 = await http.post(
        Uri.parse('https://6312f3f0a8d3f673ffc1a55b.mockapi.io/Student'),
        body: map);
    print("Responce 1 : ${responce1}");
  }

  Future<void> updateUser(id) async {
    Map map = {};
    map["name"] = nameController.text;
    map["city"] = cityController.text;
    map['avatar'] = imageController.text;

    var responce1 = await http.put(
        Uri.parse('https://6312f3f0a8d3f673ffc1a55b.mockapi.io/Student/$id'),
        body: map);
    print("Responce 1 : ${responce1}");
  }
}
